package com.ofg.loans.backoffice.api.client;

import static com.ofg.loans.application.command.RejectManualIdentificationCommand.SendNotificationType.DONT_SEND_NOTIFICATION;
import static com.ofg.loans.application.command.RejectManualIdentificationCommand.SendNotificationType.SEND_NOTIFICATION;
import static com.ofg.loans.domain.model.attachment.AttachmentType.ATTACHMENT;
import static com.ofg.loans.domain.model.client.ClientSearchCriteria.byClientNumber;
import static com.ofg.loans.util.date.DateTimeUtils.now;
import static com.ofg.loans.util.numeric.BigDecimalUtils.amount;
import static java.util.Arrays.asList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.BooleanUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Optional;
import com.google.common.io.ByteStreams;
import com.ofg.loans.api.beans.BudgetInfoBean;
import com.ofg.loans.api.beans.IdentifiedBy;
import com.ofg.loans.application.command.AddBudgetInfoToClientCommand;
import com.ofg.loans.application.command.ApplyForLineOfCreditCommand;
import com.ofg.loans.application.command.CancelPendingLineOfCreditApplicationsCommand;
import com.ofg.loans.application.command.CheckLatestLineOfCreditApplicationCommand;
import com.ofg.loans.application.command.CheckLineOfCreditApplicationCommand;
import com.ofg.loans.application.command.CommandService;
import com.ofg.loans.application.command.GetClientAddressesCommand;
import com.ofg.loans.application.command.GetClientApplicationsCommand;
import com.ofg.loans.application.command.GetClientAttachmentsCommand;
import com.ofg.loans.application.command.GetClientBudgetInfoCommand;
import com.ofg.loans.application.command.GetClientByPersonalIdCommand;
import com.ofg.loans.application.command.GetClientCommunicationCommand;
import com.ofg.loans.application.command.GetClientDistributionsCommand;
import com.ofg.loans.application.command.GetClientEventsCommand;
import com.ofg.loans.application.command.GetClientInformationCommand;
import com.ofg.loans.application.command.GetClientLatestOfflineApplicationCommand;
import com.ofg.loans.application.command.GetClientLinesOfCreditCommand;
import com.ofg.loans.application.command.GetClientOfflineApplicationsCommand;
import com.ofg.loans.application.command.GetClientOpenLoanCommand;
import com.ofg.loans.application.command.GetClientsCommand;
import com.ofg.loans.application.command.GetLatestLineOfCreditApplicationResponseCommand;
import com.ofg.loans.application.command.GetRequiredAdditionalDocumentsCommand;
import com.ofg.loans.application.command.IdentifyClientCommand;
import com.ofg.loans.application.command.MarkClientForManualIdentificationCommand;
import com.ofg.loans.application.command.RejectManualIdentificationCommand;
import com.ofg.loans.application.command.SaveAttachmentCommand;
import com.ofg.loans.application.command.SetClientCreditLimitCommand;
import com.ofg.loans.application.command.UpdateClientOfflineCommand;
import com.ofg.loans.application.command.UpdateRequiredAdditionalDocumentsCommand;
import com.ofg.loans.application.command.handlers.UpdateClientBankAccountAndAddressCommandTransformer;
import com.ofg.loans.core.bean.VoidBean;
import com.ofg.loans.core.bean.application.CheckLatestLineOfCreditApplicationResult;
import com.ofg.loans.core.bean.application.GetLatestLineOfCreditApplicationResponseBean;
import com.ofg.loans.core.bean.application.LineOfCreditApplicationBean;
import com.ofg.loans.core.bean.application.LineOfCreditApplicationsBean;
import com.ofg.loans.core.bean.budgetinfo.BudgetInfoEntryBean;
import com.ofg.loans.core.bean.budgetinfo.BudgetInfoEntryBean.Type;
import com.ofg.loans.core.bean.client.ClientAddressesBean;
import com.ofg.loans.core.bean.client.ClientAttachmentsBean;
import com.ofg.loans.core.bean.client.ClientBankAccountAndAddressBean;
import com.ofg.loans.core.bean.client.ClientBean;
import com.ofg.loans.core.bean.client.ClientInformationBean;
import com.ofg.loans.core.bean.client.ClientRegistrationBean;
import com.ofg.loans.core.bean.client.ClientsBean;
import com.ofg.loans.core.bean.client.RequiredAdditionalDocumentsBean;
import com.ofg.loans.core.bean.client.communication.CommunicationBean;
import com.ofg.loans.core.bean.client.events.ClientEventsBean;
import com.ofg.loans.core.bean.loan.LoanBean;
import com.ofg.loans.core.bean.loc.DistributionsBean;
import com.ofg.loans.core.bean.loc.LinesOfCreditBean;
import com.ofg.loans.core.bean.offline.OfflineApplicationBean;
import com.ofg.loans.domain.loc.model.application.LineOfCreditApplication;
import com.ofg.loans.domain.model.attachment.AttachmentType;
import com.ofg.loans.risk.api.ResolutionDetail;

@Path("/clients")
@Produces(APPLICATION_JSON)
@Component
public class ClientResource {

    @Autowired
    private CommandService commandService;

    @Autowired
    private UpdateClientBankAccountAndAddressCommandTransformer updateClientBankAccountAndAddressCommandTransformer;

    @GET
    @Path("/{clientNumber}/communication")
    public CommunicationBean notificationsByType(@PathParam("clientNumber") String clientNumber,
                                                 @QueryParam("type") String communicationType) {
        return commandService.execute(new GetClientCommunicationCommand(clientNumber, communicationType));
    }

    @GET
    @Path("/{clientNumber}")
    public ClientInformationBean information(@PathParam("clientNumber") String clientNumber) {
        return getClientInfo(clientNumber);
    }

    private ClientInformationBean getClientInfo(String clientNumber) {
        return commandService.execute(new GetClientInformationCommand(clientNumber));
    }

    @GET
    @Path("/{clientNumber}/addresses")
    public ClientAddressesBean clientAddresses(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetClientAddressesCommand(clientNumber));
    }


    @GET
    @Path("/{clientNumber}/applications")
    public LineOfCreditApplicationsBean clientApplications(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetClientApplicationsCommand(clientNumber));
    }

    @POST
    @Path("/{clientNumber}/applications")
    public LineOfCreditApplicationBean applyForLoan(@PathParam("clientNumber") String clientNumber,
                                            @FormParam("applicationType") @NotNull LineOfCreditApplication.ApplicationType type,
                                            @FormParam("amount") @NotNull BigDecimal amount
                                            ) {

        ClientInformationBean clientInfo = getClientInfo(clientNumber);

        ApplyForLineOfCreditCommand applyForLineOfCreditCommand =
                new ApplyForLineOfCreditCommand(clientInfo.getId(), amount, now(), type);

        return commandService.execute(applyForLineOfCreditCommand);
    }

    @POST
    @Path("/{clientNumber}/applications/{applicationId}/check")
    public CheckLatestLineOfCreditApplicationResult checkApplication(@PathParam("clientNumber") @NotNull String clientNumber,
                                                             @PathParam("applicationId") @NotNull Long applicationId) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        return commandService.execute(new CheckLineOfCreditApplicationCommand(clientInfo.getId(), applicationId, now()));
    }

    @POST
    @Path("/{clientNumber}/applications/latest/check")
    public CheckLatestLineOfCreditApplicationResult checkLatestApplication(@PathParam("clientNumber") String clientNumber) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        return commandService.execute(new CheckLatestLineOfCreditApplicationCommand(clientInfo.getId(), now()));
    }

    @GET
    @Path("/{clientNumber}/lineOfCredit")
    public LinesOfCreditBean clientLoans(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetClientLinesOfCreditCommand(clientNumber));
    }

    @GET
    @Path("/{clientNumber}/distributions")
    public DistributionsBean clientDistributions(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetClientDistributionsCommand(clientNumber));
    }

    @POST
    @Path("/{clientNumber}/identify")
    public VoidBean identifyClientManually(@PathParam("clientNumber") String clientNumber) {

        ClientInformationBean clientInfo = getClientInfo(clientNumber);

        IdentifyClientCommand identifyClientCommand = new IdentifyClientCommand(clientInfo.getId());
        identifyClientCommand.setIdentifiedBy(IdentifiedBy.MANUAL);
        identifyClientCommand.setPersonalId(clientInfo.getPersonalId());

        return commandService.execute(identifyClientCommand);
    }

    @POST
    @Path("/{clientNumber}/bankAccountAndAddress")
    @Consumes(APPLICATION_JSON)
    public VoidBean updateBankAccountAndAddress(@PathParam("clientNumber") String clientNumber, @NotNull ClientBankAccountAndAddressBean bean) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);

        return commandService.execute(updateClientBankAccountAndAddressCommandTransformer.buildUpdateCommand(clientInfo.getId(), bean));
    }

    @POST
    @Path("/{clientNumber}/rejectManualIdentification/{sendNotification}")
    public VoidBean rejectManualIdentification(@PathParam("clientNumber") String clientNumber,
                                               @PathParam("sendNotification") boolean sendNotification,
                                               @DefaultValue("") @QueryParam("comment") String comment) {

        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        RejectManualIdentificationCommand rejectIdentification =
                new RejectManualIdentificationCommand(clientInfo.getId(),
                        sendNotification ? SEND_NOTIFICATION : DONT_SEND_NOTIFICATION, comment);
        return commandService.execute(rejectIdentification);
    }

    @POST
    @Path("/{clientNumber}/budget")
    public VoidBean addBudgetInfo(@PathParam("clientNumber") String clientNumber, @FormParam("type") String type,
                                  @FormParam("amount") String amount, @FormParam("category") String category) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);

        Type entryType = Type.valueOf(type);
        BudgetInfoEntryBean incomeSource = new BudgetInfoEntryBean(null, entryType, category);
        BudgetInfoEntryBean budgetEntry = new BudgetInfoEntryBean(amount(amount), entryType, "Uncategorized");
        return commandService.execute(new AddBudgetInfoToClientCommand(clientInfo.getId(), now(), incomeSource, budgetEntry));
    }

    @POST
    @Path("/{clientNumber}/budget-incomeSources")
    public VoidBean addBudgetInfo(@PathParam("clientNumber") String clientNumber, @FormParam("type") String type,
                                  @FormParam("amount") String amount, @FormParam("category") String category, @FormParam("incomeSources") String incomeSources) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);

        AddBudgetInfoToClientCommand addBudgetInfoCommand = new AddBudgetInfoToClientCommand(clientInfo.getId());
        List<String> incomeSourceList = asList(incomeSources.split(","));
        for (String source : incomeSourceList) {
            addBudgetInfoCommand.addBudgetInfoEntryBean(new BudgetInfoEntryBean(null, Type.valueOf(type), source.trim()));
        }
        addBudgetInfoCommand.addBudgetInfoEntryBean(new BudgetInfoEntryBean(amount(amount), Type.valueOf(type), category));
        return commandService.execute(addBudgetInfoCommand);
    }


    @GET
    @Path("/{clientNumber}/budget")
    public BudgetInfoBean budgetInfo(@PathParam("clientNumber") String clientNumber, @QueryParam("allSources") Boolean allSources) {
        GetClientBudgetInfoCommand command = new GetClientBudgetInfoCommand(clientNumber);
        command.setAllSources(BooleanUtils.isTrue(allSources));
        return commandService.execute(command);
    }


    @GET
    @Path("/{clientNumber}/latestApplicationResponse")
    public GetLatestLineOfCreditApplicationResponseBean getLatestLoanApplicationResponse(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetLatestLineOfCreditApplicationResponseCommand(clientNumber));
    }

    @GET
    @Path("/{clientNumber}/attachments")
    public ClientAttachmentsBean getClientAttachments(@PathParam("clientNumber") String clientNumber) {
        GetClientAttachmentsCommand command = new GetClientAttachmentsCommand(byClientNumber(clientNumber));
        return commandService.execute(command);
    }

    @POST
    @Path("/{clientNumber}/creditlimit")
    public VoidBean setClientCreditLimit(@PathParam("clientNumber") String clientNumber,
                                         @FormParam("limit") BigDecimal limit) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        return commandService.execute(new SetClientCreditLimitCommand(clientInfo.getId(), new Date(), limit));
    }

    @POST
    @Path("/{clientNumber}/attachments")
    @Consumes(MULTIPART_FORM_DATA)
    public Response uploadAttachment(@FormDataParam("file") InputStream inputStream,
                                     @FormDataParam("file") FormDataContentDisposition fileDisposition,
                                     @FormDataParam("type") String type,
                                     //TODO: description is ignored
                                     @FormDataParam("description") String description,
                                     @PathParam("clientNumber") String clientNumber) throws IOException {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        AttachmentType attachmentType = type != null ? AttachmentType.forCamelCaseName(type) : ATTACHMENT;

        SaveAttachmentCommand command = new SaveAttachmentCommand(clientInfo.getId(), attachmentType,
                fileDisposition.getFileName(),
                ByteStreams.toByteArray(inputStream));

        commandService.execute(command);

        return Response.status(CREATED).build();
    }

    @PUT
    @Path("/{clientNumber}/identification/manual")
    public Response setToManualIdentification(@PathParam("clientNumber") String clientNumber) throws IOException {
        MarkClientForManualIdentificationCommand command = new MarkClientForManualIdentificationCommand(byClientNumber(clientNumber));
        commandService.execute(command);
        return ok().build();
    }

    @GET
    public ClientsBean getClients(@QueryParam("personal-id") String personalId) {
        GetClientsCommand command = new GetClientsCommand();
        command.setPersonalId(personalId);
        return commandService.execute(command);
    }

    @GET
    @Path("/search")
    public Response search(@QueryParam("personal-id") String personalId) {
        Optional<ClientRegistrationBean> client = commandService.execute(new GetClientByPersonalIdCommand(personalId));
        if (!client.isPresent()) {
            return status(NOT_FOUND).build();
        }
        return ok(client.get()).build();
    }

    @PUT
    @Consumes(APPLICATION_JSON)
    public Response update(@NotNull ClientBean clientBean) {
        commandService.execute(new UpdateClientOfflineCommand(clientBean));
        return ok().build();
    }

    @GET
    @Path("/{clientNumber}/offline-applications/latest")
    public Response latestOfflineApplications(@PathParam("clientNumber") String clientNumber) {
        Optional<OfflineApplicationBean> bean = commandService.execute(new GetClientLatestOfflineApplicationCommand(clientNumber));
        if (bean.isPresent()) {
            return ok(bean.get()).build();
        }
        return status(NOT_FOUND).build();
    }

    @GET
    @Path("/{clientNumber}/loans/current")
    public Response latestOpenLoan(@PathParam("clientNumber") String clientNumber) {
        Optional<LoanBean> bean = commandService.execute(new GetClientOpenLoanCommand(clientNumber));
        return bean.isPresent() ? ok(bean.get()).build() : status(NOT_FOUND).build();
    }

    @GET
    @Path("/{clientNumber}/offline-applications")
    public Response allOfflineApplications(@PathParam("clientNumber") String clientNumber) {
        return ok(commandService.execute(new GetClientOfflineApplicationsCommand(clientNumber))).build();
    }

    @PUT
    @Path("/{clientNumber}/loan-applications/cancellation/pending")
    public Response cancelPendingApplications(@PathParam("clientNumber") String clientNumber, @QueryParam("resolutionDetail") ResolutionDetail resolutionDetail) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        commandService.execute(new CancelPendingLineOfCreditApplicationsCommand(clientInfo.getId(), resolutionDetail));
        return ok().build();
    }

    @GET
    @Path("/{clientNumber}/events")
    public ClientEventsBean clientEvents(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetClientEventsCommand(clientNumber));
    }

    @GET
    @Path("/{clientNumber}/requiredAdditionalDocuments")
    @Produces(MediaType.APPLICATION_JSON)
    public RequiredAdditionalDocumentsBean getRequiredAdditionalDocuments(@PathParam("clientNumber") String clientNumber) {
        return commandService.execute(new GetRequiredAdditionalDocumentsCommand(clientNumber));
    }

    @PUT
    @Path("/{clientNumber}/requiredAdditionalDocuments")
    public Response updateRequiredAdditionalDocuments(@PathParam("clientNumber") String clientNumber,
                                                      RequiredAdditionalDocumentsBean reqAddDocsBean) {
        ClientInformationBean clientInfo = getClientInfo(clientNumber);
        commandService.execute(new UpdateRequiredAdditionalDocumentsCommand(clientInfo.getId(), reqAddDocsBean.getRequiredAdditionalDocuments()));
        return ok().build();
    }
}
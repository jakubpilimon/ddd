package com.ofg.loans.domain.loc.model

import com.ofg.loans.domain.model.account.SingleAccount
import com.ofg.loans.util.date.LocalDateUtils
import io.fourfinanceit.loc.invoicing.Invoice
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

import static com.ofg.loans.domain.model.account.TransactionType.RENEWAL
import static com.ofg.loans.domain.model.account.TransactionType.REPAYMENT
import static com.ofg.loans.domain.model.account.TransactionType.ROLLBACK
import static com.ofg.loans.domain.model.account.TransactionType.WITHDRAWAL
import static com.ofg.loans.domain.loc.model.LineOfCreditTransactionFixture.invoicing
import static com.ofg.loans.domain.loc.model.LineOfCreditTransactionFixture.minimalPayment
import static com.ofg.loans.domain.loc.model.LineOfCreditTransactionFixture.overPayment
import static com.ofg.loans.domain.loc.model.LineOfCreditTransactionFixture.renouncementRepayment
import static com.ofg.loans.domain.loc.model.LineOfCreditTransactionFixture.repayment
import static com.ofg.loans.domain.loc.model.LocTransactionFixture.transactionIsOfType
import static com.ofg.loans.domain.loc.model.LocTransactionFixture.transactionIsOfTypeAndBookingDate
import static com.ofg.loans.domain.loc.model.RenouncementPaybackAccountsFixture.onlyWithdrawalFee
import static com.ofg.loans.domain.loc.model.product.PricingFixture.fullPrincipal
import static com.ofg.loans.domain.loc.model.product.PricingFixture.pricingWithDailyInterestRateAndWithdrawalFee
import static com.ofg.loans.domain.loc.model.product.PricingFixture.pricingWithRenewalFeePercent
import static com.ofg.loans.domain.loc.model.product.PricingFixture.pricingWithWithdrawalFee
import static com.ofg.loans.domain.model.account.SingleAccount.COMMISSION_ACCRUED
import static com.ofg.loans.domain.model.account.SingleAccount.COMMISSION_INVOICED
import static com.ofg.loans.domain.model.account.SingleAccount.INTEREST_ACCRUED
import static com.ofg.loans.domain.model.account.SingleAccount.INTEREST_INVOICED
import static com.ofg.loans.domain.model.account.SingleAccount.PENALTY_ACCRUED
import static com.ofg.loans.domain.model.account.SingleAccount.PENALTY_INVOICED
import static com.ofg.loans.domain.model.account.SingleAccount.PRINCIPAL_ACCRUED
import static com.ofg.loans.domain.model.account.SingleAccount.PRINCIPAL_INVOICED
import static com.ofg.loans.domain.model.account.SingleAccount.WITHDRAWAL_FEE_ACCRUED
import static com.ofg.loans.domain.model.account.SingleAccount.WITHDRAWAL_FEE_INVOICED
import static com.ofg.loans.paymentschedule.math.util.AmountUtils.HUNDRED
import static com.ofg.loans.paymentschedule.math.util.AmountUtils.amount
import static com.ofg.loans.util.numeric.BigDecimalUtils.amount
import static java.math.BigDecimal.TEN

@Unroll
class LineOfCreditSpec extends Specification implements WithLineOfCreditFixture {

    private static final LocalDate at = LocalDateUtils.today()
    static final int INTEREST_10 = 10
    static final int FEE_5 = 5

    def 'new loc is active'() {
        expect:
            lineOfCredit().isActive()
    }

    def 'current balance on new loc is 0'() {
        expect:
            lineOfCredit().balanceToday.accruedPrincipal == 0.0
    }

    def 'interest and invoiced interest on new loc is 0'() {
        given:
            LineOfCredit loc = lineOfCredit()
        expect:
            loc.balanceToday.accruedInterest == 0.0
            loc.balanceToday.invoicedInterest == 0.0
    }

    def 'fee and invoiced fee on new loc is 0'() {
        given:
            LineOfCredit loc = lineOfCredit()
        expect:
            loc.balanceToday.accruedWithdrawalFee == 0.0
            loc.balanceToday.invoicedWithdrawalFee == 0.0
    }

    def 'principal and invoiced principal on new loc is 0'() {
        given:
            LineOfCredit loc = lineOfCredit()
        expect:
            loc.balanceToday.accruedPrincipal == 0.0
            loc.balanceToday.invoicedPrincipal == 0.0
    }

    def 'penalty and invoiced penalty on new loc is 0'() {
        given:
            LineOfCredit loc = lineOfCredit()
        expect:
            loc.balanceToday.accruedPenalties == 0.0
            loc.balanceToday.invoicedPenalties == 0.0
    }


    def 'commission and invoiced commission on new loc is 0'() {
        given:
            LineOfCredit loc = lineOfCredit()
        expect:
            loc.balanceToday.accruedCommission == 0.0
            loc.balanceToday.invoicedCommission == 0.0
    }

    def 'open amount on new loc is 0'() {
        expect:
            !lineOfCredit().balanceToday.hasOpen()
    }

    def 'next invoice date are not set for new loc'() {
        when:
            LineOfCredit loc = lineOfCredit()
        then:
            !loc.nextInvoiceDate().present
    }

    def 'next invoice date is properly set after first withdrawal'() {
        given:
            BigDecimal firstWithdrawal = BigDecimal.valueOf(100)
            LineOfCredit loc = lineOfCredit()
        when:
            withdrawToday(loc, firstWithdrawal)
        then:
            loc.nextInvoiceDate().get() == today().plusDays(loc.invoicingPricing().getFirstInvoiceAfterDays())
    }

    def 'next invoice date is properly set after first withdrawal with #withdrawalAfterSigned days after signed'() {
        given:
            BigDecimal firstWithdrawal = BigDecimal.valueOf(100)
            LineOfCredit loc = lineOfCredit()
            LocalDate withdrawalDay = today().plusDays(withdrawalAfterSigned)
        when:
            withdraw(loc, firstWithdrawal, withdrawalDay)
        then:
            loc.nextInvoiceDate().get() == withdrawalDay.plusDays(loc.invoicingPricing().getFirstInvoiceAfterDays())
        where:
            withdrawalAfterSigned | _
            0                     | _
            1                     | _
            10                    | _
            100                   | _
    }

    def 'loc is terminated'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            loc.terminate(at)
        then:
            loc.isTerminated()
    }

    def 'loc is suspended'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            loc.suspend()
        then:
            loc.isSuspended()
    }

    def 'loc is blocked'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            loc.block()
        then:
            loc.isBlocked()
    }

    def 'loc is closed'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            loc.close(CloseReason.MANUAL)
        then:
            loc.isClosed()
    }

    def 'should increase balance accordingly on first withdrawal'() {
        given:
            LineOfCredit loc = lineOfCredit()
            BigDecimal firstWithdrawal = BigDecimal.valueOf(7)
        when:
            withdrawToday(loc, firstWithdrawal)
        then:
            loc.balanceToday.accruedPrincipal == firstWithdrawal
    }

    def 'should not accumulate interests on first withdrawal'() {
        given:
            LineOfCredit loc = lineOfCreditWithProductionInterestRate()
        when:
            withdrawToday(loc, amount("1000"))
        then:
            loc.balanceToday.accruedInterest == 0.0
    }

    def 'should increase balance accordingly on next withdrawal'() {
        given:
            LineOfCredit loc = lineOfCredit()
            BigDecimal firstWithdrawal = BigDecimal.valueOf(10)
            BigDecimal secondWithdrawal = BigDecimal.valueOf(5)
        when:
            withdrawToday(loc, firstWithdrawal)
            withdrawToday(loc, secondWithdrawal)
        then:
            loc.balanceToday.accruedPrincipal == firstWithdrawal + secondWithdrawal
    }

    def 'should create withdrawal transaction when withdrawing'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            LineOfCreditTransaction tx = withdrawToday(loc, HUNDRED)
        then:
            loc.transactions.contains(tx)
            transactionIsOfTypeAndBookingDate(tx, WITHDRAWAL, today())
    }

    def 'fee is calculated on withdrawal of #amount'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithWithdrawalFee(feeRate))
        when:
            loc.withdraw(amount, today(), withdrawalFeeCalculator)
        then:
            loc.balanceToday.accruedWithdrawalFee == expectedCalculatedFee
        where:
            feeRate | amount | expectedCalculatedFee | withdrawalFeeCalculator
            0.0     | 10.0   | 0                     | withDefaultWithdrawalFeeCalculator()
            10.0    | 5.0    | 0.5                   | withDefaultWithdrawalFeeCalculator()
            11.0    | 7.0    | 0.77                  | withDefaultWithdrawalFeeCalculator()
            20.5    | 9.0    | 1.85                  | withDefaultWithdrawalFeeCalculator()
            0.0     | 10.0   | 0                     | calculatorWithClientEligibleForZeroFirstWithdrawalFee()
            10.0    | 5.0    | 0                     | calculatorWithClientEligibleForZeroFirstWithdrawalFee()
            11.0    | 7.0    | 0                     | calculatorWithClientEligibleForZeroFirstWithdrawalFee()
            20.5    | 9.0    | 0                     | calculatorWithClientEligibleForZeroFirstWithdrawalFee()
            0.0     | 10.0   | 0.7                   | calculatorWithClientEligibleForCustomFirstWithdrawalFee()
            10.0    | 5.0    | 0.35                  | calculatorWithClientEligibleForCustomFirstWithdrawalFee()
            11.0    | 7.0    | 0.49                  | calculatorWithClientEligibleForCustomFirstWithdrawalFee()
            20.5    | 9.0    | 0.63                  | calculatorWithClientEligibleForCustomFirstWithdrawalFee()
    }

    def 'open amount is decreased on repayment of #repayment'() {
        given:
            LineOfCredit loc = lineOfCreditWithPricing(10, 5)
            withdrawToday(loc, borrowed)
        when:
            loc.repay(repayment(today()), repayment,
                    strategies(INTEREST_ACCRUED, PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED))
        then:
            loc.balanceToday.totalOpen == afterRepayment
        where:
            borrowed | repayment || afterRepayment
            200.0    | 100.0     || 110.0
            200.0    | 210.0     || 0.0
    }

    def 'open amount is decreased on renouncement repayment of #repayment'() {
        given:
            LineOfCredit loc = lineOfCreditWithPricing(10, 5)
            withdrawToday(loc, borrowed)
        and:
            loc.renounce(today(), 10, onlyWithdrawalFee())
        when:
            loc.repayRenounced(renouncementRepayment(today()), repayment,
                    strategies(INTEREST_ACCRUED, PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED))
        then:
            loc.balanceToday.totalOpen == afterRepayment
        where:
            borrowed | repayment || afterRepayment
            200.0    | 100.0     || 100.0
            200.0    | 200.0     || 0.0
    }

    def 'repayment should obey the order of #repaymentStrategy'() {
        given:
            LineOfCredit loc = lineOfCreditWith(
                    pricingWithDailyInterestRateAndWithdrawalFee(INTEREST_10, FEE_5)
            )
            withdrawToday(loc, HUNDRED)
            loc.accrueInterest(TODAY)
        when:
            loc.repay(repayment(TOMORROW), HUNDRED, strategies(*repaymentStrategy))
        then:
            Balance atTomorrow = loc.getBalanceAt(TOMORROW)
            atTomorrow.accruedInterest == remainingInterest
            atTomorrow.accruedWithdrawalFee == remainingFee
            atTomorrow.accruedPrincipal == remainingPrincipal
        where:
            repaymentStrategy                                             || remainingInterest || remainingFee || remainingPrincipal
            [INTEREST_ACCRUED, PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED] || 0.0               || 5.0          || 10.0
            [WITHDRAWAL_FEE_ACCRUED, INTEREST_ACCRUED, PRINCIPAL_ACCRUED] || 0.0               || 0.0          || 15.0
            [PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED, INTEREST_ACCRUED] || 10.0              || 5.0          || 0.0
    }

    def 'should create repayment transaction when repaying'() {
        given:
            LineOfCredit loc = lineOfCredit()
            withdrawToday(loc, HUNDRED)
        when:
            LineOfCreditTransaction tx = loc.repay(repayment(today()), TEN,
                    strategies(INTEREST_ACCRUED, PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED))
        then:
            loc.transactions.contains(tx)
            transactionIsOfType(tx, REPAYMENT)
    }

    def "should create rollback transaction when rollback requested"() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            LineOfCreditTransaction tx = withdrawToday(loc, 100)
        and:
            LineOfCreditTransaction rollbackTx = loc.rollbackOn(tx, today())
        then:
            loc.transactions.contains(rollbackTx)
            transactionIsOfTypeAndBookingDate(rollbackTx, ROLLBACK, today())
    }

    def "should not do anything when trying to rollback not existing transaction"() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            loc.rollbackOn(invoicing(today()), today())
        then:
            thrown(IllegalArgumentException)
    }

    def "should restore initial balance when transaction rollbacking"() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            LineOfCreditTransaction withdrawal = withdrawToday(loc, HUNDRED)
        and:
            loc.rollbackOn(withdrawal, today())
        then:
            !loc.balanceToday.hasOpen()
    }

    def 'cannot over pay when something was invoiced'() {
        given:
            LineOfCredit loc = lineOfCredit()
            withdrawToday(loc, HUNDRED)
        and:
            setToday(loc.nextInvoiceDate)
            invoice(loc, LocalDateUtils.today())
        when:
            loc.overPay(overPayment(today()), TEN,
                    strategies(INTEREST_ACCRUED, PRINCIPAL_ACCRUED, WITHDRAWAL_FEE_ACCRUED))
        then:
            IllegalStateException e = thrown(IllegalStateException)
            e.message.matches("Cannot overPay because invoicedAmount .* > 0")
    }

    def 'should revert invoice left amount when paying minimal payment'() {
        given:
            LineOfCredit loc = lineOfCreditWith(fullPrincipal(1.0, 15.0))
            withdrawToday(loc, HUNDRED)
        and:
            setToday(loc.nextInvoiceDate.minusDays(1))
            loc.accrueInterest(today())
            setToday(loc.nextInvoiceDate)
            Invoice invoice = invoice(loc, LocalDateUtils.today()).get()
        when:
            loc.partiallyPayAndCloseInvoice(invoice, amount(50.0), minimalPayment(today()), strategies())
        then:
            loc.balanceToday.accruedWithdrawalFee == 0.0
            loc.balanceToday.totalInvoiced == 0.0
            loc.balanceToday.accruedInterest == 0.0
            loc.balanceToday.accruedPrincipal == 81.0
    }

    def 'should charge renewal fee #expectedRenewalFee for #notPaidAmount when renewalFee % is #renewalFeeRatio'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithRenewalFeePercent(renewalFeeRatio))
        when:
            loc.chargeRenewalFeeFor(notPaidAmount, TODAY)
        then:
            loc.balanceToday.accruedRenewalFee == expectedRenewalFee
        where:
            renewalFeeRatio | notPaidAmount || expectedRenewalFee
            10.0            | 50            || 5
    }

    def 'should create renewal transaction when charging renewal fee'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithRenewalFeePercent(10.0))
        when:
            Optional<LineOfCreditTransaction> tx = loc.chargeRenewalFeeFor(100.0, TODAY)
        then:
            tx.isPresent()
            loc.transactions.contains(tx.get())
            transactionIsOfTypeAndBookingDate(tx.get(), RENEWAL, TODAY)
    }

    def 'should not create renewal transaction when renewal fee percent not defined'() {
        given:
            LineOfCredit loc = lineOfCredit()
        when:
            Optional<LineOfCreditTransaction> tx = loc.chargeRenewalFeeFor(100.0, TODAY)
        then:
            !tx.isPresent()
    }

    def 'should not create renewal transaction when amount too small'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithRenewalFeePercent(renewalFeeRatio))
        when:
            Optional<LineOfCreditTransaction> tx = loc.chargeRenewalFeeFor(notPaidAmount, TODAY)
        then:
            !tx.isPresent()
        where:
            renewalFeeRatio | notPaidAmount
            0.01            | 49
            30.0            | 0.01
            0.0             | 1000
            1000            | 0.0
    }

    def 'cannot withdraw when line of credit is renounced'() {
        given:
            LineOfCredit loc = lineOfCredit()
            loc.withdraw(20.00, today(), calculatorWithClientEligibleForCustomFirstWithdrawalFee())
        and:
            loc.renounce(today(), 1, onlyWithdrawalFee())
        when:
            loc.withdraw(100.00, today(), calculatorWithClientEligibleForCustomFirstWithdrawalFee())
        then:
            IllegalStateException e = thrown()
            e.message.contains("renounced")
    }

    def 'cannot withdraw when line of credit is dormant'() {
        given:
            LineOfCredit loc = lineOfCredit()
        and:
            loc.markDormantIfInactiveFor(today(), today())
        when:
            loc.withdraw(100.00, today(), calculatorWithClientEligibleForCustomFirstWithdrawalFee())
        then:
            IllegalStateException e = thrown()
            e.message.contains("dormant")
    }

    def 'should correctly calculate accumulated withdrawal fee in first billing cycle'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithWithdrawalFee(10.00))
        when:
            loc.withdraw(firstWithdrawal, today(), withDefaultWithdrawalFeeCalculator())
        and:
            loc.withdraw(secondWIthdrawal, today(), withDefaultWithdrawalFeeCalculator())
        and:
            loc.withdraw(thirdWithdrawal, today(), withDefaultWithdrawalFeeCalculator())
        then:
            loc.accumulatedWithdrawalFeeInCurrentCycle() == expectedAccumulatedFee
        where:
            firstWithdrawal | secondWIthdrawal | thirdWithdrawal | expectedAccumulatedFee
            100.0           | 50.0             | 200.0           | 35.0
            80.0            | 21.0             | 39.0            | 14.0
            40.0            | 25.0             | 15.0            | 8.0
            20.0            | 60.0             | 22.0            | 10.2
    }

    def 'should correctly calculate accumulated withdrawal fee in subsequent billing cycle'() {
        given:
            LineOfCredit loc = lineOfCreditWith(pricingWithWithdrawalFee(10.00))
        and:
            loc.withdraw(100.00, today(), withDefaultWithdrawalFeeCalculator())
        and:
            Invoice inv = invoice(loc).get()
            setToday(inv.issueDate)
        when:
            loc.withdraw(firstWithdrawal, today(), withDefaultWithdrawalFeeCalculator())
        and:
            loc.withdraw(secondWIthdrawal, today(), withDefaultWithdrawalFeeCalculator())
        and:
            loc.withdraw(thirdWithdrawal, today(), withDefaultWithdrawalFeeCalculator())
        then:
            loc.accumulatedWithdrawalFeeInCurrentCycle() == expectedAccumulatedFee
        where:
            firstWithdrawal | secondWIthdrawal | thirdWithdrawal | expectedAccumulatedFee
            100.0           | 50.0             | 200.0           | 35.0
            80.0            | 21.0             | 39.0            | 14.0
            40.0            | 25.0             | 15.0            | 8.0
            20.0            | 60.0             | 22.0            | 10.2
    }

    LineOfCredit renouncedLineOfCredit(LocalDate renouncementDate) {
        LineOfCredit loc = lineOfCredit()
        loc.renounce(renouncementDate, 1, onlyWithdrawalFee())
        return loc
    }

    RepaymentStrategies strategies(SingleAccount... accounts) {
        return new RepaymentStrategies(accounts)
    }


    RepaymentStrategies strategies() {
        return strategies(
                PENALTY_INVOICED,
                INTEREST_INVOICED,
                WITHDRAWAL_FEE_INVOICED,
                PRINCIPAL_INVOICED,
                COMMISSION_INVOICED,
                PENALTY_ACCRUED,
                INTEREST_ACCRUED,
                WITHDRAWAL_FEE_ACCRUED,
                PRINCIPAL_ACCRUED,
                COMMISSION_ACCRUED
        )
    }


}

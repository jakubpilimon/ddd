package com.ofg.loans.domain.loc.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;
import com.ofg.loans.domain.model.account.TransactionType;
import com.ofg.loans.application.withdraw.WithdrawalFeeCalculator;
import com.ofg.loans.domain.loc.model.commission.Commission;
import com.ofg.loans.domain.loc.model.product.InvoicingPricing;
import com.ofg.loans.domain.loc.model.product.Pricing;
import com.ofg.loans.domain.model.BaseEntity;
import com.ofg.loans.domain.model.account.Entry;
import com.ofg.loans.domain.model.account.SingleAccount;
import com.ofg.loans.domain.model.client.ClientId;
import com.ofg.loans.domain.util.DateProvider;
import com.ofg.loans.domain.util.StandardDateProvider;
import com.ofg.loans.util.annotate.VisibleForHibernate;
import com.ofg.loans.util.numeric.BigDecimalUtils;
import io.fourfinanceit.loc.invoicing.AllInvoices;
import io.fourfinanceit.loc.invoicing.Invoice;
import io.fourfinanceit.loc.invoicing.OpenInvoices;
import io.fourfinanceit.loc.renouncement.Renouncement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.ofg.loans.domain.model.account.TransactionType.GENERATE_PENALTY;
import static com.ofg.loans.domain.model.account.TransactionType.LOAN_COMMISSION;
import static com.ofg.loans.domain.model.account.TransactionType.RENEWAL;
import static com.ofg.loans.domain.model.account.TransactionType.WRITEOFF;
import static com.ofg.loans.domain.events.DomainEvents.publish;
import static com.ofg.loans.domain.loc.model.CloseReason.RENOUNCEMENT;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.ACTIVE;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.BLOCKED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.CLOSED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.DORMANT;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.PRE_SUSPENDED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.RENOUNCED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.SUSPENDED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.TERMINATED;
import static com.ofg.loans.domain.loc.model.LineOfCreditStatus.VOIDED;
import static com.ofg.loans.domain.loc.model.PenaltiesSuspendedInterval.Reason;
import static com.ofg.loans.domain.model.account.AccountGroupType.COMMISSION_GROUP;
import static com.ofg.loans.domain.model.account.AccountGroupType.PENALTY_GROUP;
import static com.ofg.loans.domain.model.account.AccountGroupType.RENEWAL_FEE_GROUP;
import static com.ofg.loans.domain.model.account.SingleAccount.PRINCIPAL_ACCRUED;
import static com.ofg.loans.util.date.LocalDateUtils.today;
import static com.ofg.loans.util.numeric.BigDecimalUtils.isPositiveAmount;
import static com.ofg.loans.util.numeric.BigDecimalUtils.isZero;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;



@Audited
@Entity
@Table(name = "LINES_OF_CREDIT")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
@Slf4j
public class LineOfCredit extends BaseEntity {

    @Column(name = "CONTRACT_NUMBER", nullable = false, unique = true)
    @Getter
    private String number;

    @Id
    @GeneratedValue(generator = "LOC_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "LOC_SEQ", sequenceName = "LOC_SEQ", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @Basic(optional = false)
    @Getter
    private ClientId clientId;

    @Basic(optional = false)
    @Column(name = "SIGNED_DATE")
    @Type(type = "com.ofg.loans.infrastructure.hibernate.LocalDateUserType")
    @Getter
    private LocalDate signedDate;

    @Column(name = "TERMINATION_DATE")
    @Type(type = "com.ofg.loans.infrastructure.hibernate.LocalDateUserType")
    @Getter
    private LocalDate terminationDate;

    @Basic(optional = false)
    @Getter
    private Pricing pricing;

    @Embedded
    private AllInvoices allInvoices;

    @Embedded
    private Transactions transactions;

    @Embedded
    private DebtCommissions debtCommissions = new DebtCommissions();

    @Embedded
    private Accounts accounts;

    @Embedded
    private PenaltiesSuspendedIntervals penaltiesSuspendedIntervals;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    @Getter
    private LineOfCreditStatus status = ACTIVE;

    @Enumerated(EnumType.STRING)
    @Column(name = "CLOSE_REASON", nullable = true)
    @Getter
    private CloseReason closeReason;

    @Column(name = "COMMENTS", nullable = true, length = 4000)
    @Basic(fetch = FetchType.LAZY)
    @Getter
    @Setter
    private String comments;

    @OneToOne(mappedBy = "lineOfCredit", cascade = CascadeType.ALL)
    private Renouncement renouncement;

    @Transient
    private DateProvider dateProvider = new StandardDateProvider();

    public static LineOfCredit createActivated(ClientId clientId, Pricing pricing,
                                               DateProvider dateProvider, LocalDate signedDate, String locNumber) {
        return new LineOfCredit(clientId, locNumber, pricing, dateProvider, signedDate);
    }

    @VisibleForHibernate
    protected LineOfCredit() {
    }

    private LineOfCredit(ClientId clientId, String number, Pricing pricing,
                         DateProvider dateProvider, LocalDate signedDate) {
        this();
        this.dateProvider = dateProvider;
        this.number = number;
        this.clientId = clientId;
        this.signedDate = signedDate;
        this.pricing = pricing;
        this.accounts = new Accounts(this);
        this.transactions = new Transactions(this);
        this.penaltiesSuspendedIntervals = new PenaltiesSuspendedIntervals(this);
        this.allInvoices = new AllInvoices();
    }

    public LineOfCreditTransaction overPay(LineOfCreditTransaction transaction, BigDecimal amount, RepaymentStrategies repaymentStrategies) {
        final LineOfCreditTransaction tx = payments().overPay(transaction, amount, repaymentStrategies);
        invoices().assertAccountsAndInvoicesMatch();
        return tx;
    }

    public LineOfCreditTransaction reprocessAmountWhenRenouncing(LineOfCreditTransaction transaction, BigDecimal amount, RepaymentStrategies repaymentStrategies) {
        checkState(isRenounced(), "Line of credit is not renounced, %s", this);
        final LineOfCreditTransaction tx = payments().reprocessAmountWhenRenouncing(transaction, amount, repaymentStrategies);
        invoices().assertAccountsAndInvoicesMatch();
        return tx;
    }

    public LineOfCreditTransaction repay(LineOfCreditTransaction transaction, BigDecimal amount, RepaymentStrategies repaymentStrategies) {
        final LineOfCreditTransaction tx = payments().repay(transaction, amount, repaymentStrategies);
        invoices().assertAccountsAndInvoicesMatch();
        return tx;
    }

    public LineOfCreditTransaction repayRenounced(LineOfCreditTransaction transaction, BigDecimal amount, RepaymentStrategies repaymentStrategies) {
        checkState(isRenounced(), "Line of credit is not renounced, %s", this);
        final LineOfCreditTransaction tx = payments().repayRenounced(transaction, amount, repaymentStrategies);
        invoices().assertAccountsAndInvoicesMatch();
        return tx;
    }

    public LineOfCreditTransaction withdraw(BigDecimal withdrawalAmount, LocalDate bookingDate,
                                            WithdrawalFeeCalculator withdrawalFeeCalculator) {
        failIfTerminated("withdraw");
        failIfRenounced("withdraw");
        failIfDormant("withdraw");

        return withdrawals().withdraw(withdrawalFeeCalculator, withdrawalAmount, bookingDate);
    }

    public LineOfCreditTransaction blockAmount(BigDecimal amount, LocalDate bookingDate, LineOfCreditTransactionId transactionId) {
        failIfTerminated("blockAmount");
        final LineOfCreditTransaction blockedWithdrawal = settlements().block(amount, bookingDate, transactionId);
        publish(new LineOfCreditTransactionBlocked(id(), clientId, amount, bookingDate, transactionId));
        return blockedWithdrawal;
    }

    private Withdrawals withdrawals() {
        return new Withdrawals(this, transactions, accounts);
    }

    public LineOfCreditTransaction settleBlockedAmount(LineOfCreditTransactionId blockedWithdrawalId, WithdrawalFeeCalculator withdrawalFeeCalculator, SettlementInterestCalculator interestCalculator, LocalDate bookingDate) {
        final LineOfCreditTransaction settlement = settlements().settleBlocked(blockedWithdrawalId,
                withdrawalFeeCalculator, interestCalculator, bookingDate);
        publish(new LineOfCreditTransactionSettled(
                id(),
                blockedWithdrawalId,
                settlement.getLocTransactionId(),
                bookingDate));
        return settlement;
    }

    private Settlements settlements() {
        return new Settlements(this, transactions, accounts, interest());
    }

    /**
     * Issues an invoice if scheduled for today or overdue.
     *
     * @param issueDate
     * @param dueDateCalculator
     * @return An invoicing transaction or an empty optional if there was no need to create an invoicing transaction
     * (e.g. no withdrawals)
     * @throws IllegalStateException if the next invoice is scheduled in the future
     */
    public Optional<Invoice> invoice(LocalDate issueDate, InvoiceDueDateCalculator dueDateCalculator) {
        failIfTerminated("invoice");
        return invoices().issue(issueDate, dueDateCalculator);
    }

    private Invoices invoices() {
        return new Invoices(this, allInvoices, transactions, accounts);
    }

    public InvoicingPricing invoicingPricing() {
        return pricing.getInvoicingPricing();
    }

    public OpenInvoices remainingInvoices() {
        return invoices().remaining();
    }

    public List<Invoice> allInvoices() {
        return allInvoices.all();
    }

    public boolean hasInvoiceIssuedBefore(LocalDate date) {
        return allInvoices.all()
                .stream()
                .anyMatch(invoice -> invoice.getIssueDate().isBefore(date));
    }

    public LineOfCreditTransaction rollbackOn(LineOfCreditTransaction toRollback, LocalDate bookingDate) {
        LineOfCreditTransaction rollback = transactions.rollback(toRollback, bookingDate);
        publishLineOfCreditRollbackOccurrence(bookingDate, rollback);
        return rollback;
    }

    private void publishLineOfCreditRollbackOccurrence(LocalDate bookingDate, LineOfCreditTransaction rollback) {
        publish(new LineOfCreditTransactionRollbackOccurred(id(), bookingDate, rollback.getTransaction()));
    }

    public List<LineOfCreditTransaction> accrueInterest(LocalDate bookingDate) {
        return interest().accrue(bookingDate);
    }

    public LineOfCredit withNextInvoiceDate(LocalDate requestedNextInvoiceDate) {
        return invoices().withNextInvoiceDate(requestedNextInvoiceDate);
    }

    public void activate() {
        changeStatus(ACTIVE);
    }

    public void preSuspend() {
        publish(new LineOfCreditPreSuspended(id(), today()));
        changeStatus(PRE_SUSPENDED);
    }

    public void suspend() {
        publish(new LineOfCreditSuspended(id(), today()));
        changeStatus(SUSPENDED);
    }

    public void block() {
        publish(new LineOfCreditBlocked(id(), today()));
        changeStatus(BLOCKED);
    }

    void closeRepaid() {
        if (!getBalanceToday().hasOpen()) {
            if (isBlocked()) {
                close(CloseReason.DEBT);
            }
            if (isRenounced()) {
                close(RENOUNCEMENT);
            }
        }
    }

    public LineOfCredit close(CloseReason reason) {
        this.closeReason = reason;
        Objects.requireNonNull(reason);
        Balance balance = getBalanceToday();
        Preconditions.checkState(!balance.hasOpen(), "Can't close line of credit with open amount: " + balance);
        accounts.closeAll();
        changeStatus(CLOSED);
        publish(new LineOfCreditClosed(id()));
        return this;
    }

    public void sendToDebts(LocalDate at) {
        publish(new LineOfCreditDue(id(), at));
    }

    public void terminate(LocalDate at) {
        changeStatus(TERMINATED);
        terminationDate = at;
        publish(new LineOfCreditTerminated(id(), at));
    }

    private boolean changeStatus(LineOfCreditStatus to) {
        if (this.status != to) {
            transitionValid(to);
            log.info("Transiting {} -> {} in {}", this.status, to, this);
            this.status = to;
            return true;
        } else {
            return false;
        }
    }

    private void transitionValid(LineOfCreditStatus to) {
        checkArgument(
                canChangeStatusTo(to),
                "Transition %s -> %s is invalid, allowed %s -> %s for %s",
                this.status, to, to.allowedFrom(), to, id);
    }

    public boolean canChangeStatusTo(LineOfCreditStatus to) {
        return this.status.canBeChangedTo(to);
    }

    @PreUpdate
    @PrePersist
    @PostLoad
    public void validate() {
        accounts.validate();
        invoices().assertAccountsAndInvoicesMatch();
        getBalanceToday().validate();
        debtCommissions.validate(accounts);
    }

    @Override
    public Long getId() {
        return id;
    }

    public LineOfCreditId id() {
        return new LineOfCreditId(getId());
    }

    public LineOfCreditNo no() {
        return new LineOfCreditNo(getNumber());
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @deprecated Use #nextInvoiceDate
     */
    @Deprecated
    public LocalDate getNextInvoiceDate() {
        return allInvoices.getNextInvoiceDate();
    }

    public Optional<LocalDate> nextInvoiceDate() {
        return Optional.ofNullable(allInvoices.getNextInvoiceDate());
    }

    public boolean isActive() {
        return status == ACTIVE;
    }

    public boolean isPreSuspended() {
        return status == PRE_SUSPENDED;
    }

    public boolean isSuspended() {
        return status == SUSPENDED;
    }

    public boolean isBlocked() {
        return status == BLOCKED;
    }

    public boolean isTerminated() {
        return status == TERMINATED;
    }

    public boolean isVoided() {
        return status == VOIDED;
    }

    public boolean isClosed() {
        return status == CLOSED;
    }

    public boolean isOpen() {
        return !isTerminated() && !isVoided() && !isClosed();
    }

    public Balance getBalanceToday() {
        return new Balance(accounts, dateProvider.today());
    }

    public Balance getBalanceAt(LocalDate effective) {
        return new Balance(accounts, effective);
    }

    public boolean nothingIsInvoicedAt(LocalDate effective) {
        return isZero(getBalanceAt(effective).getTotalInvoiced());
    }

    public Optional<LineOfCreditTransaction> addDebtCommission(Commission commission) {
        if (!debtCommissions.commissionAllowedAfterTerminated(commission)) {
            failIfTerminated("add commission");
        }
        if (debtCommissions.attemptAdd(commission)) {
            commission.setLineOfCredit(this);
            return Optional.of(createCommissionTransaction(commission));
        }
        return Optional.empty();
    }

    public ImmutableList<Commission> getDebtCommissions() {
        return debtCommissions.list();
    }

    public List<LineOfCreditTransaction> accruePenalties(LocalDate bookingDate) {
        return penalties().accrue(bookingDate);
    }

    private Penalties penalties() {
        return new Penalties(this);
    }

    public void stopPenalties(LocalDate startDate, Reason reason) {
        penaltiesSuspendedIntervals.stopPenalties(startDate, reason);
        log.info("Line of credit stopped accruing penalties: {}", this);
    }

    public void startPenalties(LocalDate endDate, Reason reason) {
        penaltiesSuspendedIntervals.startPenalties(endDate, reason);
        log.info("Line of credit started accruing penalties: {}", this);
    }

    public void cancelSuspendedPenalties(LocalDate cancelDate, Reason reason) {
        penaltiesSuspendedIntervals.cancelSuspendedPenalties(cancelDate, reason);
        log.info("Line of credit cancelled suspended penalties: {}", this);
    }

    public boolean arePenaltiesSuspended(LocalDate bookingDate) {
        return penaltiesSuspendedIntervals.isPenaltySuspended(bookingDate);
    }

    private LineOfCreditTransaction createCommissionTransaction(Commission commission) {
        final LocalDate bookingDate = commission.getBookingDate();
        final LineOfCreditTransaction commissionTransaction =
                transactions.book(LOAN_COMMISSION,
                        "Commission of type " + commission.getType(),
                        accounts.accrue(bookingDate, commission.getAmount(), COMMISSION_GROUP)
                ).completed(bookingDate);
        publish(new CommissionAdded(id(), commission.getId(), commission.getBookingDate()));
        return commissionTransaction;
    }

    public List<Entry> listEntries(SingleAccount singleAccount) {
        return accounts.listEntries(singleAccount);
    }

    private void failIfTerminated(String action) {
        Preconditions.checkState(!isTerminated(), "Unable to %s on terminated LOC %s", action, id);
    }

    private void failIfRenounced(String action) {
        Preconditions.checkState(!isRenounced(), "Unable to %s on renounced LOC %s", action, id);
    }

    private void failIfDormant(String action) {
        Preconditions.checkState(!DORMANT.equals(status), "Unable to %s on dormant LOC %s", action, id);
    }

    public Optional<BigDecimal> getLatestWithdrawalAmount() {
        return transactions
                .findLatestWithdrawalTxBeforeOrThatDay(dateProvider.today())
                .map(locTransaction -> locTransaction.amountOnAccount(PRINCIPAL_ACCRUED));
    }

    public BigDecimal accumulatedWithdrawalFeeInCurrentCycle() {
        LocalDate billingCycleStart = transactions.findLatestInvoicingDateTx().orElse(getSignedDate());
        if (!dateProvider.today().isAfter(billingCycleStart)) {
            return transactionsInPeriod(Range.singleton(billingCycleStart)).accumulatedWithdrawalFee();
        }
        return transactionsInPeriod(Range.closed(billingCycleStart, dateProvider.today())).accumulatedWithdrawalFee();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("clientId", clientId != null ? clientId.asLong() : null)
                .add("status", status)
                .add("openAmount", accounts != null ? getBalanceToday().getTotalOpen() : null)
                .add("accounts", accounts)
                .toString();
    }

    public AccountOperations transactionsInPeriod(Range<LocalDate> within) {
        return new AccountOperations(transactions, within);
    }

    public LineOfCreditTransaction partiallyPayAndCloseInvoice(Invoice invoice, BigDecimal totalAmount, LineOfCreditTransaction locTransaction,
                                                               RepaymentStrategies repaymentStrategies) {
        checkState(allInvoices.contains(invoice), "Cannot find invoice: " + invoice);
        return payments().partiallyPayAndCloseInvoice(invoice, totalAmount, locTransaction, repaymentStrategies);
    }

    private Payments payments() {
        return new Payments(this, transactions, accounts);
    }

    public Optional<LineOfCreditTransaction> chargeRenewalFeeFor(BigDecimal notPaidAmount, LocalDate bookingDate) {
        return pricing.renewalFeeFor(notPaidAmount)
                .filter(BigDecimalUtils::isPositiveAmount)
                .map(renewalFee -> {
                    publish(new RenewalFeeCharged(id(), now(), notPaidAmount, renewalFee));
                    return transactions.book(RENEWAL,
                            "Renewal fee for amount " + notPaidAmount,
                            accounts.accrue(bookingDate, renewalFee, RENEWAL_FEE_GROUP))
                            .completed(bookingDate);
                });
    }

    public void markDormantIfInactiveFor(LocalDate from, LocalDate to) {
        if (isActive() && !transactions.hasWithdrawals(from, to)) {
            changeStatus(DORMANT);
        }
    }

    public void writeOff(SingleAccount singleAccount, BigDecimal amount,
                         LocalDate bookingDate, Optional<String> reason) {
        checkArgument(isPositiveAmount(amount), "Writeoff amount (%s) should be positive", amount);
        reason.ifPresent(reasonStr -> checkState(!reasonStr.isEmpty(), "When writeoff reason is present it cannot be empty"));

        LineOfCreditTransaction tx = bookTransaction(WRITEOFF, bookingDate,
                ImmutableMap.of(singleAccount, amount.negate()),
                writeOffDetails(reason));
        publishWriteOffOccurredEvent(tx, singleAccount, amount);
    }

    private void writeOffAll(SingleAccount singleAccount, LocalDate bookingDate, Optional<String> reason) {
        BigDecimal amountOnAccount = accounts.balanceOn(singleAccount, bookingDate);
        if (isPositiveAmount(amountOnAccount)) {
            writeOff(singleAccount, amountOnAccount, bookingDate, reason);
        }
    }

    private void publishWriteOffOccurredEvent(LineOfCreditTransaction tx, SingleAccount singleAccount, BigDecimal amount) {
        publish(new WriteOffOccurred(id(), clientId, tx.getTransactionId(), tx.getBookingDate(),
                singleAccount, amount));
    }

    private LineOfCreditTransaction bookTransaction(TransactionType transactionType, LocalDate bookingDate,
                                                    Map<SingleAccount, BigDecimal> accountChanges,
                                                    String details) {

        List<Entry> allChangesEntries = accountChanges.entrySet().stream()
                .flatMap(accountChange -> {
                    SingleAccount singleAccount = accountChange.getKey();
                    BigDecimal accountChangeAmount = accountChange.getValue();
                    return accounts.charge(singleAccount, accountChangeAmount, bookingDate).stream();
                }).collect(toList());

        //noinspection unchecked
        return transactions.book(
                transactionType,
                details,
                allChangesEntries
        ).completed(bookingDate);
    }

    private String writeOffDetails(Optional<String> reasonMaybe) {
        final StringBuilder sb = new StringBuilder("Writeoff");
        reasonMaybe.ifPresent(reason -> sb.append(": ").append(reason));
        return sb.toString();
    }

    public LineOfCredit voidLine(LocalDate effective) {
        removeWithdrawalTransaction();
        interest().writeOffAll(effective);
        final Balance balance = getBalanceAt(effective);
        Preconditions.checkState(!balance.hasInvoiced(), "Can't void line with invoices: " + remainingInvoices());
        Preconditions.checkState(!balance.hasAccrued(), "Can't void line with open amount: " + balance);
        accounts.closeAll();
        changeStatus(VOIDED);
        sendToDebts(effective);
        publish(new LineOfCreditVoided(id(), effective));
        return this;
    }

    private Interest interest() {
        return new Interest(this, transactions, accounts);
    }

    private void removeWithdrawalTransaction() {
        List<LineOfCreditTransaction> withdrawals = transactions.findWithdrawals();
        throwsExceptionOnMoreThanOneTransaction(withdrawals);
        withdrawals.stream().findFirst().ifPresent(this::removeTransaction);
    }

    private void throwsExceptionOnMoreThanOneTransaction(Collection<LineOfCreditTransaction> withdrawals) {
        Preconditions.checkState(withdrawals.size() <= 1,
                "Trying to void line with multiple withdrawals: " + withdrawals);
    }

    private void removeTransaction(LineOfCreditTransaction tx) {
        transactions.remove(tx);
        accounts.removeAllEntriesFrom(tx);
    }

    public boolean hasWithdrawals(LocalDate to) {
        return transactions.hasWithdrawals(signedDate, to);
    }

    public List<BigDecimal> getWithdrawalsAmountsHistory() {
        return transactions.findWithdrawalsAmountsHistory();
    }

    public BigDecimal calculateFullRepaymentAmount(LocalDate effectiveDate) {
        return getBalanceAt(effectiveDate).getTotalOpen()
                .add(interest().calculateMissingUpTo(effectiveDate))
                .add(penalties().calculateMissingUpTo(effectiveDate.minusDays(1)));
    }

    /**
     * Used by Vaadin UI
     */
    public BigDecimal getFullRepaymentAmountToday() {
        return calculateFullRepaymentAmount(dateProvider.today());
    }

    public void updateFirstInvoiceDateStartingFrom(LocalDate date) {
        nextInvoiceDate().orElseGet(() -> {
            final int firstInvoiceAfterDays = pricing.getInvoicingPricing().getFirstInvoiceAfterDays();
            final LocalDate nextInvoiceDate = date.plusDays(firstInvoiceAfterDays);
            allInvoices.setNextInvoiceDate(nextInvoiceDate);
            return nextInvoiceDate;
        });
    }

    public Renouncement renounce(LocalDate bookingDate, int maxDaysToRepayEverything, RenouncementPaybackAccounts paybackAccounts) {
        invoices().cancelAll(bookingDate);
        changeStatus(RENOUNCED);
        paybackAccounts.getList().stream()
                .forEach(account -> writeOffAll(account, bookingDate, Optional.of("Renouncement")));
        this.renouncement = new Renouncement(bookingDate, bookingDate.plusDays(maxDaysToRepayEverything), this);
        closeRepaid();
        return this.renouncement;
    }

    public Optional<Renouncement.State> getRenouncement() {
        return Optional.ofNullable(renouncement)
                .map(Renouncement::getState);
    }

    public boolean isRenounced() {
        return RENOUNCED.equals(status) || RENOUNCEMENT.equals(closeReason);
    }

    public boolean isRenouncementDue(LocalDate at) {
        return isRenounced() && renouncement.isDueAt(at);
    }

    Optional<LocalDate> getLastPenalizationDateBefore(LocalDate bookingDate) {
        return transactions.getLastPenalizationDateBefore(bookingDate);
    }

    boolean hasAccumulatedPenaltyTxOn(LocalDate bookingDate) {
        return transactions.findAccumulatedPenaltyTxOn(bookingDate).isPresent();
    }

    LineOfCreditTransaction accrueDailyPenalty(LocalDate bookingDate, BigDecimal penaltyAmount) {
        return transactions.book(
                GENERATE_PENALTY,
                "Daily penalty for " + number + " (" + status + ")",
                accounts.accrue(bookingDate, penaltyAmount, PENALTY_GROUP));
    }

}

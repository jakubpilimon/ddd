package com.ofg.loans.application.command.handlers;

import static com.ofg.loans.util.date.DateTimeUtils.getNumberOfMinutes;
import static com.ofg.loans.util.date.DateTimeUtils.now;
import static org.apache.commons.lang.StringUtils.isEmpty;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ofg.loans.application.command.ApplyForLineOfCreditCommand;
import com.ofg.loans.core.bean.application.LineOfCreditApplicationBean;
import com.ofg.loans.domain.loc.model.application.LineOfCreditApplication;
import com.ofg.loans.domain.loc.model.application.LineOfCreditApplication.Status;
import com.ofg.loans.domain.model.EntityRepository;
import com.ofg.loans.domain.model.client.Client;
import com.ofg.loans.domain.model.loan.LineOfCreditApplicationRepository;
import com.ofg.loans.domain.model.loan.validation.borrow.ApplicationValidatorFactory;
import com.ofg.loans.domain.model.loan.validation.borrow.ValidateLineOfCreditApplication;
import com.ofg.loans.domain.util.WebCodeUtils;
import com.ofg.loans.domain.workflow.loan.LineOfCreditApplicationWorkflow;
import com.ofg.loans.domain.workflow.loan.LineOfCreditApplicationWorkflowBuilder;
import com.ofg.loans.risk.api.ResolutionDetail;

@Component
@Scope("prototype")
class ApplyForLineOfCreditHandler extends AbstractCommandHandler<ApplyForLineOfCreditCommand, LineOfCreditApplicationBean> {

    public static final Logger log = LoggerFactory.getLogger(ApplyForLineOfCreditHandler.class);

    private static final int MAX_EXISTING_WEB_CODE_AGE_IN_MINUTES = 60 * 2;

    private static final String DEFAULT_WEB_CODE = "1234";

    @Autowired
    protected EntityRepository repository;

    @Autowired
    LineOfCreditApplicationRepository lineOfCreditApplicationRepository;

    @Autowired
    ApplicationValidatorFactory validateApplicationOfferFactory;

    @Autowired
    protected LineOfCreditApplicationWorkflowBuilder workflowBuilder;

    protected Client client;
    
    protected ApplyForLineOfCreditCommand command;

    @Value("${testing.web.code.default.on:false}")
    private boolean useDefaultWebCode;

    protected LineOfCreditApplication createApplication() {
        final BigDecimal amount = command.getAmount();
        LineOfCreditApplication application = new LineOfCreditApplication(client, amount, command.getApplicationType());
        application.setReferer(command.getReferer());
        application.setTypeDetail(command.getApplicationTypeDetail());
        application.setLineOfCreditPurpose(command.getLoanPurpose());
        application.setAutoRepay(command.isAutoRepay());
        return application;
    }

    @Override
    protected LineOfCreditApplicationBean doExecute(ApplyForLineOfCreditCommand command) {
        this.command = command;
        client = repository.getRequired(Client.class, command.getClientId());
        repository.lockForWrite(client);
        LineOfCreditApplication application = createApplication();
        repository.persist(application);

        LineOfCreditApplicationWorkflow workflow = workflowBuilder.build(application);

        if (!isEmpty(command.getManualRejectReason())) {
            workflow.reject(command.getDate(), ResolutionDetail.MANUAL_REJECT, command.getManualRejectReason());
            return LineOfCreditApplicationBean.rejected(application.getId(), application.getResolutionAsText());
        }

        String webCode = useDefaultWebCode ? DEFAULT_WEB_CODE : webCode();
        cancelOtherOpenApplications(application);
        validate(application, workflow);
        if (application.isClosed()) {
            return LineOfCreditApplicationBean.rejected(application.getId(), application.getResolutionAsText());
        }

        application.setWebCode(webCode, command.getDate());

        workflow.open(command.getDate());
        return LineOfCreditApplicationBean.confirmed(application.getId(), application.getWebCode());
    }

    protected void validate(LineOfCreditApplication application, LineOfCreditApplicationWorkflow workflow) {
        if (command.isSkipVerification()) {
            return;
        }
        ResolutionDetail detail = application.apply(new ValidateLineOfCreditApplication(command.getDate(), validateApplicationOfferFactory));
        if (detail != null) {
            workflow.reject(command.getDate(), detail, "");
        }
    }

    protected String webCode() {
        String webCode = findExistingWebCode();
        return webCode == null ? WebCodeUtils.getCode() : webCode;
    }

    protected void cancelOtherOpenApplications(LineOfCreditApplication application) {
        lineOfCreditApplicationRepository
                .listApplications(application.getClient(), Status.OPEN)
                .stream()
                .filter(openApplication -> !openApplication.equals(application))
                .forEach(openApplication -> workflowBuilder
                        .build(openApplication)
                        .cancel(now(), ResolutionDetail.NOT_LATEST));
    }

    protected String findExistingWebCode() {
        LineOfCreditApplication latestOpenApplication = lineOfCreditApplicationRepository.latestApplication(client, Status.OPEN);
        if (latestOpenApplication == null) {
            return null;
        }
        int ageInMinutes = getNumberOfMinutes(latestOpenApplication.getWebCodeCreateDate(), command.getDate());
        return ageInMinutes < MAX_EXISTING_WEB_CODE_AGE_IN_MINUTES ? latestOpenApplication.getWebCode() : null;
    }
}
